import random

class Chromosome:
    def __init__(self, sourceDict, encoded, dad=None, mom=None, mutation=None):
        self.key = dict()
        if dad == None or mom == None or dad.getScore() == 0 or mom.getScore() == 0:
            self.makeRandom()
        else:
            self.makeChild(dad.getDict(), mom.getDict(), mutation)
        self.score = 0
        self.isComplete = 1
        self.calculateScore(sourceDict, encoded)
        self.decoded = ''
        for i in range(len(encoded)):
            if encoded[i] in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz":
                self.decoded = self.decoded + self.key[encoded[i]]
            else:
                self.decoded = self.decoded + encoded[i]

    def makeRandom(self):
        values = ''.join(random.sample('ABCDEFGHIJKLMNOPQRSTUVWXYZ', len('ABCDEFGHIJKLMNOPQRSTUVWXYZ')))
        values = values + ''.join(random.sample('abcdefghijklmnopqrstuvwxyz', len('abcdefghijklmnopqrstuvwxyz')))        
        keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        for i in range(len(keys)):
            self.key[keys[i]] = values[i]

    def makeChild(self, dad, mom, mutation):
        keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        seen = set()
        leftUpKeys = ''
        leftDownKeys = ''
        leftUpValues = ''
        leftDownValues = ''
        self.key = dict()
        for i in keys:
            if random.randint(0, 1) == 0 and dad[i] not in seen:
                self.key[i] = dad[i]
                seen.add(dad[i])
            elif mom[i] not in seen:
                self.key[i] = mom[i]
                seen.add(mom[i])
            else:
                if i in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                    leftUpKeys = leftUpKeys + i
                else:
                    leftDownKeys = leftDownKeys + i
        for i in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            if i not in seen:
                leftUpValues = leftUpValues + i
        for i in 'abcdefghijklmnopqrstuvwxyz':
            if i not in seen:
                leftDownValues = leftDownValues + i
        upValues = ''.join(random.sample(leftUpValues, len(leftUpValues)))
        downValues = ''.join(random.sample(leftDownValues, len(leftDownValues)))
        for i in range(len(leftUpKeys)):
            self.key[leftUpKeys[i]] = upValues[i]
        for i in range(len(leftDownKeys)):
            self.key[leftDownKeys[i]] = downValues[i]
        for i in range(len(keys)):
            if random.random() < mutation:
                if i < len(keys) / 2:
                    index = random.randint(0, len(keys)/2 - 1)
                    self.key[keys[i]], self.key[keys[index]] = self.key[keys[index]], self.key[keys[i]]
                else:
                    index = random.randint(len(keys)/2, len(keys) - 1)
                    self.key[keys[i]], self.key[keys[index]] = self.key[keys[index]], self.key[keys[i]]

    def calculateScore(self, sourceDict, encoded):
        self.score = 0
        word = ''
        for i in encoded:
            if i in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz":
                word = word + self.key[i]
            elif word != '':
                if word in sourceDict.keys():
                    self.score += len(word)
                else:
                    self.isComplete = 0
                word = ''

    def getDict(self):
        return self.key

    def getScore(self):
        return self.score

    def getDecoded(self):
        return self.decoded

    def getIsComplete(self):
        return self.isComplete

class Population:
    def __init__(self, sourceDict, encoded, n = 100, mutation = 0.1):
        self.n = n
        self.encoded = encoded
        self.sourceDict = sourceDict
        self.dad = 0
        self.mutation = mutation
        self.defaultMutation = mutation
        self.highScore = 0
        self.isInLocalMax = False
        self.chromosomes = [Chromosome(sourceDict, encoded) for i in range(n)]
        self.chances = [self.chromosomes[i].getScore() for i in range(len(self.chromosomes))]
        self.sumChance = sum(self.chances)
        self.streak = 0
        while self.chromosomes[0].getIsComplete() == 0:
            self.evolve()

    def evolve(self):
        if not self.isInLocalMax:
            self.evolved = self.chromosomes[:int(len(self.chromosomes) / 2)]
            self.chances = self.chances[:int(len(self.chromosomes) / 2)]
        self.sumChance = sum(self.chances)
        if not self.isInLocalMax:
            for i in range(self.n - len(self.evolved)):
                lottery = random.random()
                for i in range(len(self.chances)):
                    if lottery > self.chances[i] / self.sumChance:
                        lottery -= self.chances[i] / self.sumChance
                    else:
                        self.dad = i
                        break
                self.evolved.append(self.mate(self.evolved[self.dad], self.evolved[(self.dad+1)%len(self.evolved)]))
                self.chances.append(self.evolved[-1].getScore())
        else:
            for i in range(len(self.evolved)):
                self.evolved[i] = self.mate(self.evolved[i], self.evolved[(i+1)%len(self.evolved)])
                self.chances[i] = self.evolved[i].getScore()
        self.chromosomes = sorted(self.evolved, key = lambda x: x.getScore(), reverse=True)
        if self.chromosomes[0].getScore() > self.highScore:
            self.streak = 0
        else:
            self.streak += 1
        self.highScore = self.chromosomes[0].getScore()
        self.sumChance = sum(self.chances)

        if self.streak > 10 and self.streak > self.chromosomes[0].getScore() / 100:
            self.mutation = 0.7
            self.isInLocalMax = True
            self.streak = 0
        else:
            self.isInLocalMax = False
            self.mutation = self.defaultMutation

    def mate(self, dad, mom):
        return Chromosome(self.sourceDict, self.encoded, dad, mom, self.mutation)
    
    def getBestDecode(self):
        return self.chromosomes[0].getDecoded()

class DicBuilder:
    def __init__(self, filename):
        file = open(filename, 'r')
        content = file.read()
        self.cleansed = dict()
        word = ''
        for i in content:
            if i in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz":
                word = word + i
            elif word != '':
                if word in self.cleansed.keys():
                    self.cleansed[word] += 1
                else:
                    self.cleansed[word] = 1
                word = ''

    def getDictionary(self):
        return self.cleansed

class Decoder:
    def __init__(self, encoded):
        self.encoded = encoded
    
    def decode(self):
        self.DB = DicBuilder('./global_text.txt')
        self.nation = Population(self.DB.getDictionary(), self.encoded, n=400, mutation=0.01)
        self.decoded = self.nation.getBestDecode()
        return self.decoded

# enc = open('encoded_text.txt').read()
# d = Decoder(enc)
# dec = d.decode()
# print(dec)